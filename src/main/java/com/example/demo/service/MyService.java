package com.example.demo.service;

import com.example.demo.config.CosCntMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class MyService {

    private final CosCntMap cosCntMap;

    @Autowired
    public MyService(CosCntMap cosCntMap) {
        this.cosCntMap = cosCntMap;
    }
}
