package com.example.demo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "cos")
public class CosCntMap {

    private boolean enabled;
    private CosCnt cnt;
    private Map<String, CosCnt> clients;

    public CosCnt getCnt() {
        return cnt;
    }

    public void setCnt(CosCnt cnt) {
        this.cnt = cnt;
    }

    public Map<String, CosCnt> getClients() {
        return clients;
    }

    public void setClients(Map<String, CosCnt> clients) {
        this.clients = clients;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
